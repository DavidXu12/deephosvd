input = im2double(imread('noisy_Sig20.00_stream.pgm'))*255;
meo_final = zeros(512,512);
out_final = zeros(512,512);
for i = 1:505
    load(['./result_stream_v3/out_Slice_',num2str(i),'.mat'])
    load(['./result_stream_v3/meoff_Slice_',num2str(i),'.mat'])
    meo_final = meo_final + meoff;
    out_final = out_final + out;
end

out = zeros(512,512);
meoffn=find(meo_final==0);
out(meoffn)=input(meoffn);
meo_final(meoffn)=1;
out=out_final./meo_final;
out=out/255; %convert back
imwrite(out,['hdnoiseDeep_25_stream_v3_sigma',num2str(20),'f',num2str(8),num2str(rand),'.bmp'],'bmp')    