#!/bin/bash
#
#SBATCH --job-name=DHOH
#SBATCH --array=1-505
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=2GB
#SBATCH --time=8:59:10
#SBATCH --output=Gen_%A_%a.out
#SBATCH --mail-type=END
#SBATCH --mail-user=sx475@nyu.edu



module purge
module load matlab/2017a

cd $SCRATCH/DeepHOSVD

if [ "$SLURM_JOBTMP" == "" ]; then
    export SLURM_JOBTMP=/state/partition1/$USER/$$
    mkdir -p $SLURM_JOBTMP
fi

export MATLAB_PREFDIR=$(mktemp -d $SLURM_JOBTMP/matlab-XXXX)

echo
echo "Hostname: $(hostname)"
echo

cat<<EOF | srun matlab -nodisplay
sl = $SLURM_ARRAY_TASK_ID
noisy=im2double(imread('noisy_Sig20.00_barbara.pgm'));
ref = noisy;
out = hdnoiseDeepNewSlice_v5_25_v3(noisy, noisy, 8, 20,sl, 'barbara');

ref = noisy;
out = hdnoiseDeepNewSlice_v5_25_v3(noisy, noisy, 8, 20,sl, 'lena');
exit
EOF
 
rm -rf $SLURM_JOBTMP/*
