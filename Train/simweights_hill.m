m=512;n=512;f=8;ct=0;
noisy=im2double(imread('hill_sig20.bmp'));
noisy=noisy*255;

clean=im2double(imread('./images/hill.png'));
clean=clean*255;
 sig=20;
 h=3*sig*sig*f*f;
 fil=zeros(f,f,2);
 thres=sig*sqrt(2*log(f*f*2));
for io=x_i
    io
    for jo=randi(50):5:505
        imax=min(m-f+1,io+20);
        jmax=min(n-f+1,jo+20);
        imin=io;
        jmin=jo;
        g=noisy(io:io+f-1,jo:jo+f-1);
        for ii=imin:imax
            for jj=jmin:jmax
                if (ii~=io)||(jj~=jo)
                    gtemp=noisy(ii:ii+f-1,jj:jj+f-1);                           
                    w=sum(sum((g-gtemp).^2));  
                    if w<h
                    ct=ct+1;
                    v(1,ct)=io;v(2,ct)=jo;v(3,ct)=ii;v(4,ct)=jj;
                    fil(:,:,1)=g;
                    fil(:,:,2)=gtemp;
                    [a,b] = hosvd(fil);
                    a(a<thres&a>-thres)=0;
                    fil=tprod(a,b);
                    errw(ct,1)=(sum(sum((fil(:,:,1)-clean(io:io+f-1,jo:jo+f-1)).^2))/(f*f)+sum(sum((fil(:,:,2)-clean(ii:ii+f-1,jj:jj+f-1)).^2))/(f*f))/(255*255);
                    end
                end
            end
        end
    end
end
save(['hill_PositAndWeights_xi_',num2str(x_i),'.mat'],'v','errw');

                    
                    