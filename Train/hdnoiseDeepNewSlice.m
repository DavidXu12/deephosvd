function [out]=hdnoiseDeepNewSlice(input,ref,f,h,slice)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the implementation of HOSVD image denoising
%  input: image to be filtered
%  f: patchsize
%  h: degree of noise(sigma)
%  io,jo:indication of the position of the top left point of the given patch
%  the K here is set to 30
%  code written by Shenghe Xu from XJTU (shenghex@gmail.com)
%  according to 'Image Denoising Using the Higher Order Singular Value Decomposition' by Ajit Rajwade,Anand Rangarajan and Arunava Banerjee
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Size of the image
[m,n]=size(input);
load('net1.mat');
%data=zeros(1,129);
%imwrite(input,['D:\ipt\hosvd\t\insigma',num2str(h),'.bmp'],'bmp')
%convert to the form with max value of 255
input=input*255;
ref=ref*255;
out=zeros(m,n);
sig=h;
h=3*h*h*f*f;
meoff=zeros(m,n);
for io=slice
    io
    for jo=1:1:n-f+1
        g=ref(io:io+f-1,jo:jo+f-1);
        
        
        posit=zeros(2,1700);
        numk=0;
        imax=min(m-f+1,io+20);
        jmax=min(n-f+1,jo+20);
        imin=max(1,io-20);
        jmin=max(1,jo-20);
        ws=ones(1,1700);
        ws=ws*(h+10);
        
        for i=imin:imax
            for j=jmin:jmax
                
                
                gtemp=ref(i:i+f-1,j:j+f-1);
                w=sum(sum((g-gtemp).^2));
                if  w<h
                    
                    numk=numk+1;
                    posit(:,numk)=[i,j];
                    
                    %                     for jp=1:8
                    %                         data(1,((jp-1)*8+1):jp*8)=input(io+jp-1,jo:jo+7)/255;
                    %                         data(1,((jp-1)*8+1+64):jp*8+64)=input(i+jp-1,j:j+7)/255;
                    %                     end
                    if i==io&&j==jo
                        ws(numk)=-1;
                    else
                        temp_img_in = cat(1,g,gtemp);
                        ws(numk) = predict(net,temp_img_in);

                        %                     data(1,129)=1;
                        %                     w1probs = 1./(1 + exp(-data*w1)); w1probs = [w1probs  ones(1,1)];
                        %                     w2probs = 1./(1 + exp(-w1probs*w2)); w2probs = [w2probs ones(1,1)];
                        %                     w3probs = 1./(1 + exp(-w2probs*w3)); w3probs = [w3probs  ones(1,1)];
                        %                     targetout = exp(w3probs*w_class);
                        %                     targetout = targetout./repmat(sum(targetout,2),1,10);
                        %                     [tt1,tt2]=max(targetout);
                        %                     ws(numk)=sum(targetout.*[0 1 2 3 4 5 6 7 8 9])+5*tt2;
                        
                    end
                end
                
                
            end
        end
        if numk>30
            numk=30;
        end
        filposit=zeros(2,numk);
        fil=zeros(f,f,numk);
        
        for wi=1:numk
            [~,minidex]=min(ws);
            
            filposit(:,wi)=posit(:,minidex);
            ws(minidex)=h+10;
        end
        for fi=1:numk
            fil(:,:,fi)=input(filposit(1,fi):filposit(1,fi)+f-1,filposit(2,fi):filposit(2,fi)+f-1);
        end
        
        if numk>3
            [a,b] = hosvd(fil);
        else
            dcttemp=dctmtx(f);
            if numk==1
                b=cell(1,2);
                bt=cell(1,2);
                b{1}=dcttemp';
                b{2}=dcttemp';
                bt{1}=dcttemp;
                bt{2}=dcttemp;
                a=tprod(fil,bt);
            end
            if numk>1
                b=cell(1,3);
                bt=cell(1,3);
                b{1}=dcttemp';
                b{2}=dcttemp';
                b{3}=dctmtx(numk)';
                bt{1}=dcttemp;
                bt{2}=dcttemp;
                bt{3}=dctmtx(numk);
                a=tprod(fil,bt);
            end
        end
        thres=sig*sqrt(2*log(f*f*numk));
        a(a<thres&a>-thres)=0;
        fil=tprod(a,b);
        
        for ft=1:numk
            x=filposit(1,ft);
            y=filposit(2,ft);
            out(x:x+f-1,y:y+f-1)=out(x:x+f-1,y:y+f-1)+fil(:,:,ft);
            meoff(x:x+f-1,y:y+f-1)=meoff(x:x+f-1,y:y+f-1)+1;
            
        end
        
        
    end
end
save(['meoff_Slice_',num2str(slice),'.mat'],'meoff')
save(['out_Slice_',num2str(slice),'.mat'],'out')
%meoffn=find(meoff==0);
%out(meoffn)=input(meoffn);
%meoff(meoffn)=1;
%out=out./meoff;
%out=out/255; %convert back
%imwrite(out,['hdnoiseDeepNewsigma',num2str(sig),'f',num2str(f),num2str(rand),'.bmp'],'bmp')
