sigma = 20;
% y        = im2double(imread('./images/couple.png'));
% randn('seed', 0);                          %% generate seed
% z        = y + (sigma/255)*randn(size(y));
% imwrite(z,['couple_sig',num2str(20),'.bmp'],'bmp')   
% 
% y        = im2double(imread('./images/hill.png'));
% randn('seed', 0);                          %% generate seed
% z        = y + (sigma/255)*randn(size(y));
% imwrite(z,['hill_sig',num2str(20),'.bmp'],'bmp')   
% 
% y        = im2double(imread('./images/man.png'));
% randn('seed', 0);                          %% generate seed
% z        = y + (sigma/255)*randn(size(y));
% imwrite(z,['man_sig',num2str(20),'.bmp'],'bmp')   
% 
% 
% y        = im2double(imread('./images/peppers256.png'));
% randn('seed', 0);                          %% generate seed
% z        = y + (sigma/255)*randn(size(y));
% imwrite(z,['peppers256_sig',num2str(20),'.bmp'],'bmp')   

images = dir(fullfile('./images','*.jpg'));
randn('seed', 0); 
for i = 1:length(images)
    baseName = images(i).name;
    fullName = fullfile('./images',baseName);
    y = im2double(imread(['./noisy_images/orig_',num2str(i),'.bmp']));
    z = y + (sigma/255)*randn(size(y));
    outname1 = ['./noisy_images/sig_',num2str(sigma),'_',num2str(i),'.bmp'];
    imwrite(z,outname1,'bmp') 
    %outname2 = ['./noisy_images/orig_',num2str(i),'.bmp'];
   % imwrite(y,outname2,'bmp') 
end