f=8;ct=0;
sig=20;
h=3*sig*sig*f*f;
p1 = zeros(f,f,15e6);
p2 = zeros(f,f,15e6);
errw = zeros(15e6,1);
patch_per_img = 15e6/25;
for read_i = 1:25
    read_i
    noisy=im2double(imread(['./noisy_images/sig_',num2str(sig),'_',num2str(read_i),'.bmp']));
    noisy=noisy*255;
    %m=512;n=512;
    [m,n] = size(noisy);
    
    clean=im2double(imread(['./noisy_images/orig_',num2str(read_i),'.bmp']));
    clean=clean*255;
    
    
    fil=zeros(f,f,2);
    thres=sig*sqrt(2*log(f*f*2));
    m_t = randperm(m-f+1);
    n_t = randperm(n-f+1);
    img_p_cnt = 1;
    for ioo=1:m-f+1
        io = m_t(ioo);
        for joo= 1:n-f+1
            jo = n_t(joo);
            imax=min(m-f+1,io+20);
            jmax=min(n-f+1,jo+20);
            imin=io;
            jmin=jo;
            g=noisy(io:io+f-1,jo:jo+f-1);
            point_count = 1;
            iirdp = randperm(imax-imin+1);
            jjrdp = randperm(jmax-jmin+1);
            iiis = imin:imax;
            jjjs = jmin:jmax;
            for ii1=1:imax-imin+1
                ii = iiis(iirdp(ii1));
                for jj1=1:jmax-jmin+1
                    jj = jjjs(jjrdp(jj1));
                    if ((ii~=io)||(jj~=jo) ) && (img_p_cnt<=patch_per_img) &&(point_count<=2)
                        
                        gtemp=noisy(ii:ii+f-1,jj:jj+f-1);
                        w=sum(sum((g-gtemp).^2));
                        if w<h 
                            img_p_cnt = img_p_cnt + 1;
                            point_count = point_count+ 1;
                            ct=ct+1;
                            v(1,ct)=io;v(2,ct)=jo;v(3,ct)=ii;v(4,ct)=jj;
                            fil(:,:,1)=g;
                            fil(:,:,2)=gtemp;
                            p1(:,:,ct) = g;
                            p2(:,:,ct) = gtemp;
                            [a,b] = hosvd(fil);
                            a(a<thres&a>-thres)=0;
                            fil=tprod(a,b);
                            errw(ct,1)=(sum(sum((fil(:,:,1)-clean(io:io+f-1,jo:jo+f-1)).^2))/(f*f)+sum(sum((fil(:,:,2)-clean(ii:ii+f-1,jj:jj+f-1)).^2))/(f*f))/(255*255);
                        end
                    end
                end
            end
        end
    end
end
%save(['couple_PositAndWeights_xi_',num2str(x_i),'.mat'],'v','errw');
p1 = uint8(p1);
p2 = uint8(p2);
save('25_v3_p1_15e6.mat','p1')
save('25_v3_p2_15e6.mat','p2')
save('25_v3_errw_15e6.mat','errw')


