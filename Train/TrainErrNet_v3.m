if exist('TrainErrNetv3Dataa.mat', 'file') == 2
    load('TrainErrNetv3Dataa.mat')
else
    load('25_v3_p1_15e6.mat')
    load('25_v3_p2_15e6.mat')
    load('25_v3_errw_15e6.mat')
    errs = errw;
    
    inputs = cat(1,p1,p2);
    inputs_new = zeros(16,8,1,15e6,'uint8');
    
    for i = 1:15e6
        inputs_new(:,:,1,i) = inputs(:,:,i);
    end
    
    
    idx_perm = randperm(15e6);
    inputs = inputs_new(:,:,1,idx_perm);
    errs = errs(idx_perm);
    
    XTrain = inputs(:,:,1,1:14e6);
    YTrain = errs(1:14e6);
    
    XValidation = inputs(:,:,1,(14e6+1):15e6);
    YValidation = errs((14e6+1):15e6);
    save('TrainErrNetv3Dataa.mat','XTrain','YTrain','XValidation','YValidation')
end

disp('data ready')



imageSize = [16,8,1];
layers = [
    imageInputLayer(imageSize)
    
   
    
    fullyConnectedLayer(16*8)
    reluLayer
    
    fullyConnectedLayer(16*6)
    reluLayer
    
    fullyConnectedLayer(16*5)
    reluLayer
    
    fullyConnectedLayer(16*4)
    reluLayer
    
    fullyConnectedLayer(16*2)
    reluLayer
    
    fullyConnectedLayer(16)
    reluLayer
    
    fullyConnectedLayer(1)
    regressionLayer];


miniBatchSize  = 1000;

validationFrequency = 25e4/miniBatchSize;

options = trainingOptions('sgdm', ...
    'MiniBatchSize',miniBatchSize, ...
    'MaxEpochs',2, ...
    'InitialLearnRate',1e-3, ...
    'LearnRateSchedule','piecewise', ...
    'LearnRateDropFactor',0.1, ...
    'LearnRateDropPeriod',2, ...
    'Shuffle','every-epoch', ...
    'ValidationData',{XValidation,YValidation}, ...
    'ValidationFrequency',validationFrequency, ...
    'Plots','training-progress', ...
    'Verbose',false);


[net,trace] = trainNetwork(XTrain,YTrain,layers,options);


