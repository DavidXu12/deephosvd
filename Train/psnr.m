function PSNR = psnr(f1, f2)

e = f1 - f2;
b = mean(mean(e.^2));
PSNR = 10*log10(1/b);
end

