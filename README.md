# Project Title

This is the code for "Deep Neural Network based Similar Patch Selection for Image Denoising". 

## Run Code
All codes are written in Matlab. 
To train neural network, in folder 'train', run 'Gen_p_err_all_v3.m', then run 'TrainErrNet_v3.m' and save the network parameters.
To evaluate on slurm based HPC, in folder 'Evaluate', run 'DeepHOSVD_slice_v5_25_5_v3.sh', then run 'CombineImage_h.m' with the the corresponding folder names.
The code GetPSNR.m can be used to calculate PSNR values. 



## Acknowledgments

The original code for HOSVD is from Professor Ajit Rajwade's homepage.

